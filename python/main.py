import json
import socket
import sys

class Piece(object):
    def __init__(self):
        self.length = 0
        self.switch = False
        self.radius = 0
        self.angle = 0
    
    def setLength(self, l):
        self.length = l

    def setRadius(self, r):
        self.radius = r

    def setAngle(self, a):
        self.angle = a

    def setSwitch(self, s):
        self.switch = False;

    def __unicode__(self):
        print __str__(self)

    def __str__(self):
        print self.length, "/", self.switch, "/", self.radius, "/", self.angle
        return ""

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.pieces = []

    def getNextPiece(self, i):
        if(i == len(self.pieces)-1):
            return 0
        return i+1

    def lengthUntilNextTurn(self, i):
        l = 0
        while(self.pieces[i].radius == 0):
            l += self.pieces[i].length
            i = self.getNextPiece(i)
        return l

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        for car in data:
            if car['id']['name'] == self.name:
                location = car['piecePosition']['pieceIndex']
                if(self.pieces[location].radius == 0):
                    if(self.lengthUntilNextTurn(location) > 300): # More like 350...
                        self.throttle(0.9)
                    else:
                        self.throttle(0.7)
                else:
                    self.throttle(0.6)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def handleInit(self, data):
        for piece in data['race']['track']['pieces']:
            p = Piece()
            if 'length' in piece:
                p.setLength(piece['length'])
            if 'radius' in piece:
                p.setRadius(piece['radius'])
            if 'angle' in piece:
                p.setAngle(piece['angle'])
            if 'switch' in piece:
                p.setSwitch(piece['switch'])
            self.pieces.append(p)
            print p

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.handleInit
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
